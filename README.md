### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run --seed
```


## Run the server
> adonis serve --dev